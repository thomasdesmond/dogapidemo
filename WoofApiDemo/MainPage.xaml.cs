﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AppCenter.Analytics;
using Microsoft.AppCenter.Crashes;
using Newtonsoft.Json;
using Xamanimation;
using Xamarin.Forms;

namespace WoofApiDemo
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

        async void HandleGetADogPicture(object sender, System.EventArgs e)
        {
            await DogPhoto.FadeTo(0, 1000);
            Spinner.IsRunning = true;
            DogPhoto.Source = null;
            var client = new HttpClient();

            var dogApiAddress = "https://dog.ceo/api/breeds/image/random";
            var uri = new Uri(dogApiAddress);

            DogImage dogData = new DogImage();
            var response = await client.GetAsync(uri);
            if (response.IsSuccessStatusCode)
            {
                var jsonContent = await response.Content.ReadAsStringAsync();
                dogData = JsonConvert.DeserializeObject<DogImage>(jsonContent);
                Analytics.TrackEvent("Successfully Retrived Random Dog");

            }

            DogPhoto.Source = dogData.DogPhotoUrl;
            await DogPhoto.FadeTo(1, 1500);

            Spinner.IsRunning = false;
        }

        void Handle_Animate(object sender, System.EventArgs e)
        {
            DogPhoto.Animate(new ShakeAnimation());
        }
    }
}
