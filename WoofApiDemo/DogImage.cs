﻿using System;
using Newtonsoft.Json;

namespace WoofApiDemo
{
    public class DogImage
    {
        public string Status { get; set; }

        [JsonProperty("Message")]
        public string DogPhotoUrl { get; set; }
    }
}
